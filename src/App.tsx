import { useState } from "react";
import "./App.css";
import Button from "@mui/material/Button";
import LocalPhoneIcon from "@mui/icons-material/LocalPhone";
import ContactsIcon from "@mui/icons-material/Contacts";
import PlayArrowIcon from "@mui/icons-material/PlayArrow";
import AddCircleIcon from "@mui/icons-material/AddCircle";
import PauseIcon from "@mui/icons-material/Pause";
import { Container, IconButton } from "@mui/material";
import { FaDog } from "react-icons/fa";
import ImageUrl from "./assets/hamas.jpeg";
import { io } from "socket.io-client";
import Webcam from "react-webcam";
import { play } from "./functions/playFunction";
import { callConntact } from "./functions/callFunction";
import { Uploader } from "uploader"; // Installed by "react-uploader".
import { UploadButton } from "react-uploader";
import UploadImage from "./components/uploadImage";

// Initialize once (at the start of your app).
const uploader = Uploader({
  apiKey: "free", // Get production API keys from Upload.io
});

// Configuration options: https://upload.io/uploader#customize
const options = { multi: true };

function App() {
  const [camera, setCamera] = useState(true);

  return (
    <div className="App">
      <IconButton
        onClick={() => {
          camera ? setCamera(false) : setCamera(true);
        }}
      >
        {camera ? <PlayArrowIcon /> : <PauseIcon />}
      </IconButton>
      <Container>
        {camera ? (
          <img
            src={ImageUrl}
            style={{ width: "300px", borderRadius: "10px" }}
            alt="demoImage"
          />
        ) : (
          <Webcam style={{ width: "300px", borderRadius: "10px" }} />
        )}
        <Container
          sx={{
            display: "flex",
            flexDirection: "row-reverse",
            justifyContent: "space-around",
            alignItems: "center",
          }}
        >
          <Button
            startIcon={<FaDog />}
            sx={{
              backgroundColor: "white",
              fontSize: "20px",
              color: "black",
              borderRadius: "10px",
              border: "solid 1px black",
              margin: "10px",
              height: "40px",
              width: "140px",
            }}
            variant="outlined"
            onClick={play}
          >
            נביחות
          </Button>
          <Button
            startIcon={<ContactsIcon sx={{ width: "20px", height: "20px" }} />}
            sx={{
              backgroundColor: "white",
              fontSize: "20px",
              color: "black",
              borderRadius: "10px",
              border: "solid 1px black",
              margin: "10px",
              width: "140px",
              height: "40px",
            }}
            variant="outlined"
            onClick={callConntact}
          >
            איש קשר
          </Button>
        </Container>
        <Button
          endIcon={<LocalPhoneIcon sx={{ width: "30px", height: "30px" }} />}
          style={{}}
          sx={{
            backgroundColor: "red",
            fontSize: "30px",
            color: "white",
            width: "300px",
            height: "100px",
            borderRadius: "10px",
            "&:focus": {
              backgroundColor: "red",
            },
            "&:hover": {
              backgroundColor: "red",
            },
          }}
          variant="contained"
        >
          משטרה
        </Button>
      </Container>
      <UploadImage />
    </div>
  );
}

export default App;
