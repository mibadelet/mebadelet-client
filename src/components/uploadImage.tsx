import { IconButton } from "@mui/material";
import AddCircleIcon from "@mui/icons-material/AddCircle";
import { Uploader } from "uploader"; // Installed by "react-uploader".
import { UploadButton } from "react-uploader";

// Initialize once (at the start of your app).
const uploader = Uploader({
  apiKey: "free", // Get production API keys from Upload.io
});

// Configuration options: https://upload.io/uploader#customize
const options = { multi: true };

function UploadImage() {
  return (
    <UploadButton
      uploader={uploader}
      options={options}
      onComplete={(files) =>
        console.log(files.map((x) => x.fileUrl).join("\n"))
      }
    >
      {({ onClick }) => (
        <IconButton onClick={onClick}>
          <AddCircleIcon
            sx={{
              width: "70px",
              height: "70px",
              marginTop: "220px",
              marginLeft: "260px",
            }}
          />
        </IconButton>
      )}
    </UploadButton>
  );
}

export default UploadImage;
