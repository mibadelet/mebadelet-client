const requestOptions = {
  method: "POST",
  headers: {
    "Content-Type": "application/json",
    Authorization:
      "Basic NjFmNzA3ZGMtODNjZS00NjRkLTg3ZjMtYWU4NjlmNDUzYjg3OllUUU84Mks0VDBHY0VGVnA4YnJrakE9PQ==",
  },
  body: '{ \
    "method": "ttsCallout", \
    "ttsCallout": { \
      "cli": "+447520651963", \
      "domain": "pstn", \
      "destination": { "type": "number", "endpoint": "+972507742288" }, \
      "locale": "en-US", \
      "prompts": "#tts[Someone is at the door. Someone is at the door. Someone is at the door.]" \
    }}',
};

export const callConntact = () => {
  fetch("https://calling.api.sinch.com/calling/v1/callouts", requestOptions)
    .then((response) => response.json())
    .then((result) => console.log(result))
    .catch((error) => console.log("error", error));
};
